package com.js.studentmanagerspringboot;

import com.alibaba.fastjson.JSON;
import com.js.studentmanagerspringboot.entity.PlanSys;
import com.js.studentmanagerspringboot.entity.User;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;

@SpringBootTest
class StudentManagerSpringBootApplicationTests {

    @Test
    void contextLoads() {
        PlanSys p = new PlanSys(1,"李四","buasdfj ",new Date(System.currentTimeMillis()),null,"1");


        User user = new User(1,"zs","123456","111","1233");
        System.out.println(JSON.toJSON(p));
        System.out.println(JSON.toJSON(user));
    }

}
