package com.js.studentmanagerspringboot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.js.studentmanagerspringboot.entity.PlanSys;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface PlanDao extends BaseMapper<PlanSys> {

}
