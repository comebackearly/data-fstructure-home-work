package com.js.studentmanagerspringboot.mapper;

import com.js.studentmanagerspringboot.entity.User;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface UserMapper {

//    查询用户信息
    @Select("select * from user")
    public List<User> select();

//    注册用户
    @Insert("insert into  user values (null,#{username},#{password},#{image},#{email},#{role})")
    public void register(User user);

//    修改用户信息
    public Integer edit(User user);

//   根据用户姓名进行查询
    @Select("select * from user where username = #{username}")
    public User selectByName(String username);

    //   根据用户id进行查询
    @Select("select * from user where id = #{id}")
    public User selectById(Integer id);

    //   根据id删除
    @Delete("delete from user where id = #{id}")
    public void deleteById(int id);

    //    根据多个id删除
    public void deleteByIds(List<Integer> ids);

    //    分页查询
    public List<User> selectPage(@Param("currentpage") Integer currentPage,
                                    @Param("pagesize") Integer pageSize,
                                 @Param("username") String inputUserName);

    //    查询总条数
    public Integer selectTotal( @Param("username") String inputUserName);

    @Select("select * from user where username = #{username} and password = #{password}")
    User selectByUserNameAndPassword(String username,String password);
}
