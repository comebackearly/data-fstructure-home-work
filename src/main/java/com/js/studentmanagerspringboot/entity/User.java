package com.js.studentmanagerspringboot.entity;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("user")
public class User {
    private Integer id;
    private String username;
    private String password;
    //    头像
    private String image;
    //    邮箱
    private String email;
}
