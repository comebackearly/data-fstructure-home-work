package com.js.studentmanagerspringboot.controller;

import com.js.studentmanagerspringboot.common.Result;
import com.js.studentmanagerspringboot.entity.PlanSys;
import com.js.studentmanagerspringboot.service.impl.PlanServiceImpl;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;


@RestController
@RequestMapping("plan")
public class PlanController {
    @Resource
    private PlanServiceImpl planService;

    @GetMapping("list")
    public Result list(){
        return planService.list();
    }

    @GetMapping("listByName")
    public Result listByName(String name){
        return planService.listByName(name);
    }

    @GetMapping("listById")
    public Result listById(Integer id){
        return planService.list();
    }

    @DeleteMapping("delete")
    public Result delete(Integer id){
        return planService.delete(id);
    }

    @PostMapping("edit")
    public Result edit(PlanSys planSys){
        return planService.edit(planSys);
    }

    @PostMapping("add")
    public Result add(@RequestBody PlanSys planSys){
        return planService.add(planSys);
    }

}
