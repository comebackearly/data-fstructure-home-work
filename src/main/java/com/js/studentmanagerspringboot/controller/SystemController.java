package com.js.studentmanagerspringboot.controller;
import com.js.studentmanagerspringboot.common.Constants;
import com.js.studentmanagerspringboot.common.Result;
import com.js.studentmanagerspringboot.dto.UserDto;
import com.js.studentmanagerspringboot.entity.User;
import com.js.studentmanagerspringboot.service.impl.UserServiceImpl;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;


/*
* 实现系统功能
* 老师登录登出
* 学生登录注册
* */
@Controller
@RequestMapping("/system")
public class SystemController {

    @Resource
    private UserServiceImpl userService;

//    登录
    @PostMapping("/login")
    public String login( UserDto userDto){
        Result result = userService.select(userDto);
//        将dto封装进结果集
        if (result == null) {
            return "redirect:/login.html";
        }else {
            return "redirect:/index.html";
        }
    }



    @PostMapping("/register")
    public Result register(@RequestBody UserDto userDto){
        if (userDto!=null){
            //        将dto封装进结果集
            return Result.success(userService.register(userDto));
        }else {
            return Result.error(Constants.CODE_500,"用户信息不能为空！");
        }
    }

////    修改用户信息
//    @PostMapping("/editUser")
//    public Result editUser(@RequestBody UserDto userDto){
//        return null;
//    }

////    根据用户姓名进行修改
//    @GetMapping("/editByName")
//    public Result selectByName(@RequestParam(value = "username") String username){
//        if (username==null){
//            return Result.error(Constants.CODE_401,"用户姓名不能为空");
//        }else {
//            return Result.success(userService.selectByName(username));
//        }
//    }
//    根据用户id查询
//    @GetMapping("/selectById/{id}")
//    public Result selectById(@PathVariable Integer id){
//            return Result.success(userService.selectById(id));
//    }
}
