package com.js.studentmanagerspringboot.common;

public enum RoleEnum {
    //    超级管理员
    ROLE_ADMIN,
    //    教师
    ROLE_TEACHER,
    //    学生
    ROLE_STUDENT,
}
