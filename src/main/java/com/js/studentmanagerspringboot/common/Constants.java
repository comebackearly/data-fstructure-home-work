package com.js.studentmanagerspringboot.common;

//状态码
public interface Constants {
//    成功
    String CODE_200 = "200";
//    失败
    String CODE_500 = "500";
//    权限不足
    String CODE_401 = "401";
//    参数不足
    String CODE_400 = "400";
//    其他业务异常
    String CODE_600 = "600";

//    dict数据类型
    String DICT_TYPE_ICON = "icon";


}
