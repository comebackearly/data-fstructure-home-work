package com.js.studentmanagerspringboot.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.xmlbeans.impl.common.NameUtil;

/*
*
* 接口统一返回包装类
*
* */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Result {
//    code字段 根据code是否成功
    private String code;
//    通用消息
    private String msg;
//   后台需要携带的数据类型
    private Object data;

//  成功的返回结果
    public static Result success(){
        return new Result(Constants.CODE_200,"", null);
    }
//    有数据的成功
    public static Result success(Object data){
        return new Result(Constants.CODE_200,"", data);
    }
//    错误
    public static Result error(String code,String msg){
        return new Result(code,msg, null);
    }
    public static Result error(){
        return new Result(Constants.CODE_500,"系统错误", null);
    }
}
