package com.js.studentmanagerspringboot.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.js.studentmanagerspringboot.common.Result;
import com.js.studentmanagerspringboot.entity.PlanSys;
import com.js.studentmanagerspringboot.mapper.PlanDao;
import com.js.studentmanagerspringboot.service.PlanService;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PlanServiceImpl implements PlanService {
    @Autowired
    private PlanDao planDao;

    /**
     * 得到所有的列表
     * @return
     */
    @Override
    public Result list() {
        List<PlanSys> planSys = planDao.selectList(null);
        return Result.success(planSys);
    }

    /**
     * 删除数据
     * @return
     */
    @Override
    public Result delete(Integer id) {
        planDao.deleteById(id);
        return Result.success();
    }

    @Override
    public Result edit(PlanSys planSys) {
        planDao.updateById(planSys);
        return  Result.success();
    }

    /**
     * 查询
     * @param name
     * @return
     */
    @Override
    public Result listByName(String name) {
        QueryWrapper<PlanSys> wrapper = new QueryWrapper<>();
        wrapper.eq("name", name);
        List<PlanSys> planSys = planDao.selectList(wrapper);
        return Result.success(planSys);
    }

    @Override
    public Result add(PlanSys planSys) {
        int insert = planDao.insert(planSys);
        if (insert>0) {
            return Result.success();
        }
        return Result.error();
    }
}
