package com.js.studentmanagerspringboot.service.impl;
import cn.hutool.core.bean.BeanUtil;
import com.js.studentmanagerspringboot.common.Constants;
import com.js.studentmanagerspringboot.common.Result;
import com.js.studentmanagerspringboot.dto.UserDto;
import com.js.studentmanagerspringboot.entity.User;
import com.js.studentmanagerspringboot.mapper.UserMapper;
import com.js.studentmanagerspringboot.service.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Resource
    private UserMapper userMapper;

    @Override
    public Result select(UserDto userDto) {
        User user = userMapper.selectByUserNameAndPassword(userDto.getUsername(),userDto.getPassword());
        if(user != null ){
            return Result.success(user);
        }
//        List<User> userList = userMapper.select();
        //   查询user表全部数据 逐一判断
//        if (userList!=null){
//            for (User user : userList) {
//                if (user.getUsername().equals(userDto.getUsername()) && user.getPassword().equals(userDto.getPassword())){
////                true:忽略大小写
//                    BeanUtil.copyProperties(user,userDto,true);
////                    将登录用户信息的userid和password设置为token的载荷和标志
////                    String token = TokenUtils.genToken( user.getId().toString(),user.getPassword());
//////                    将token封装进实体类
////                    userDto.setToken(token);
//////                    获取登录用户的角色
//                    return userDto;
//                }
//            }
//        }
        return null;
    }

    @Override
    public User register(UserDto userDto) {
        User user = new User();
        BeanUtil.copyProperties(userDto,user);
        userMapper.register(user);
        return user;
    }

    @Override
    public User selectByName(String username) {
        return null;
    }

    @Override
    public User selectById(Integer id) {
        return null;
    }

    @Override
    public void deleteById(int id) {

    }

    @Override
    public void deleteByIds(List<Integer> ids) {

    }

}
