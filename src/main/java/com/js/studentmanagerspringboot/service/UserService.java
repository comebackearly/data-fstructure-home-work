package com.js.studentmanagerspringboot.service;

import com.js.studentmanagerspringboot.common.Result;
import com.js.studentmanagerspringboot.dto.UserDto;
import com.js.studentmanagerspringboot.entity.User;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface UserService {
//    查询全部用户
    public Result select(UserDto userDto);
//    注册用户
    public User register(UserDto userDto);

    //   根据用户姓名进行查询
    public User selectByName(String username);
    //   根据用户id进行查询
    public User selectById(Integer id);

    //   根据id删除
    public void deleteById(int id);

    //    根据多个id删除
    public void deleteByIds(List<Integer> ids);
}
