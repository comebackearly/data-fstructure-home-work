package com.js.studentmanagerspringboot.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.js.studentmanagerspringboot.common.Result;
import com.js.studentmanagerspringboot.entity.PlanSys;

public interface PlanService {
    Result list();

    Result delete(Integer id);

    Result edit(PlanSys planSys);

    Result listByName(String name);

    Result add(PlanSys planSys);

}
