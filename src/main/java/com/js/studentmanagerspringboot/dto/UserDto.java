package com.js.studentmanagerspringboot.dto;

import lombok.Data;

import java.util.List;

@Data
public class UserDto {
    private Integer id;
    private String username;
    private String password;
//    头像
    private String image;
//    邮箱
    private String email;
//    token
    private String token;

}
