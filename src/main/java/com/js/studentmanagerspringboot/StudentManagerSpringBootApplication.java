package com.js.studentmanagerspringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class StudentManagerSpringBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(StudentManagerSpringBootApplication.class, args);
    }

}
